from django.contrib import admin
from receipts.models import Account, Receipt, ExpenseCategory


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "number",
    )


@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = (
        "vendor",
        "total",
        "tax",
        "date",
    )


@admin.register(ExpenseCategory)
class ExpenseAdmin(admin.ModelAdmin):
    list_display = ("name",)
