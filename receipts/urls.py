from django.urls import path
from . import views

urlpatterns = [
    path("", views.home, name="home"),
    path("create/", views.create_receipt, name="create_receipt"),
    path("accounts/", views.accounts_list, name="account_list"),
    path("categories/", views.category_list, name="category_list"),
    path("categories/create/", views.create_category, name="create_category"),
    path("accounts/create/", views.create_account, name="create_account"),
]
